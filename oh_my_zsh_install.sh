#!/bin/bash

## Install Zsh ##

# Needed packages
if [[ $USER == "root" ]]; then
	apt update -qq && \
	apt install -yqq \
		wget \
		git \
		zsh
  echo "> Installed needed packages"
fi
## End Install Zsh ##

## Install oh-my-zsh ##
wget -q https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
echo "> Installed oh-my-zsh"

# zsh-autosuggestions
git clone -q https://github.com/zsh-users/zsh-autosuggestions "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}"/plugins/zsh-autosuggestions
echo "> Added zsh-autosuggestions plugin"

# zsh-syntax-highlighting
git clone -q https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}"/plugins/zsh-syntax-highlighting
echo "> Added zsh-syntax-highlighting plugin"

# EXTRA THEMES
./extra_themes.sh

# modify .zshrc

# Make zsh the default shell
if [[ $USER == "root" ]]; then
	cat zshrc-root > ~/.zshrc
	echo "> Added custom .zshrc"
	chsh -s "$(which zsh)"
	echo "> zsh is now the default shell"
else
	cat zshrc-user > ~/.zshrc
	echo "> Added custom .zshrc"
	echo 'To make oh-my-zsh your default shell execute chsh -s $(which zsh) as root'
fi

## End Install oh-my-zsh ##
