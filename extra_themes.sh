#!/usr/bin/env bash
echo "> Adding extra themes..."
# powerlevel10k install
# download if it doesn't exist
if [ ! -d "$HOME/.config/powerlevel10k" ]; then
  git clone -q --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}"/themes/powerlevel10k
  printf "\t* Added powerlevel10k theme\n"
fi

# other extra themes
while IFS= read -r line
do
  IFS=', ' read -r -a array <<< "$line"
  file_name="${array[0]}"
  theme_url="${array[1]}"
  dest_path="$HOME/.oh-my-zsh/custom/themes/$file_name.zsh-theme"
  # download theme if doesn't exist
  if [ ! -f "$dest_path" ]; then
    touch "$dest_path"
    wget -q -O "$HOME/.oh-my-zsh/custom/themes/$file_name.zsh-theme" "$theme_url"
    printf '\t* Added %s theme\n' "$file_name"
  fi
done < <(grep -v '^ *#' < extra-themes)

echo "> Finish adding extra themes"
