# Oh my ZSH install sctipt #

Simple script to install Oh my ZSH, along with its requirements. The script also installs some plugins and additional themes and adds a custom configuration file.

## Plugins ##

* [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)
* [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)

## Extra themes ##

* [Powerlevel10k](https://github.com/romkatv/powerlevel10k)
* [Daivasmara](https://github.com/Daivasmara/daivasmara.zsh-theme)
* [Lambda V ZSH](https://github.com/vkaracic/lambdav-zsh-theme)
* [Punctual](https://github.com/dannynimmo/punctual-zsh-theme)
* [Lambda Gitster](https://github.com/ergenekonyigit/lambda-gitster)
* [Chaotic beef](https://github.com/ARtoriouSs/chaotic-beef-zsh-theme)
* [Passion](https://github.com/ChesterYue/ohmyzsh-theme-passion) (no funciona bien en Linux, pero molaría para macOS)
* [xxf](https://gist.github.com/xfanwu/18fd7c24360c68bab884)
* [Pi](https://github.com/tobyjamesthomas/pi)